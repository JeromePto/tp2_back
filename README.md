**Name:** Jérôme Petot  

**Title**: TP2 of node  

**Run instruction:**  
`yarn/npm install` then  
- to run the first part: `npm run part-1`
- to run the second part: `npm run start`

**Introduction:** A project to discover express.js and typescript