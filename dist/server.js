"use strict";
var express = require("express");
var path = require('path');
var MetricsHandler = require('./metrics.ts');
var app = express();
var port = process.env.PORT || '8080';
app.set("views", path.join(__dirname, 'views'));
app.set("view engine", 'ejs');
app.use(express.static(path.join(__dirname, 'public')));
app.get("/hello/:name", function (req, res) {
    // res.send("Hello " + req.params.name);
    res.render('hello.ejs', { name: req.params.name });
});
app.get('/metrics.json', function (req, res) {
    MetricsHandler.MetricsHandler.get(function (err, result) {
        if (err)
            throw err;
        res.status(200).json(result);
    });
});
app.get('/', function (req, res) {
    res.write("Hello world");
    res.end();
});
app.listen(port, function (err) {
    if (err) {
        throw err;
    }
    console.log("server listening on " + port);
});
