const express = require("express");
const path = require('path');

const MetricsHandler = require('./metrics.ts');

const app = express();
const port: string = process.env.PORT || '8080';

app.set("views", path.join(__dirname, 'views'));
app.set("view engine", 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

app.get("/hello/:name", (req: any, res: any) => {
    // res.send("Hello " + req.params.name);
    res.render('hello.ejs', {name: req.params.name});
});

app.get('/metrics.json', (req: any, res: any) => {
    MetricsHandler.MetricsHandler.get((err: Error | null, result?: any) => {
        if (err) throw err;
        res.status(200).json(result);
    })
});

app.get('/', (req: any, res: any) => {
    res.write("Hello world");
    res.end();
});

app.listen(port, (err: Error) => {
    if (err) {
        throw err;
    }
    console.log(`server listening on ${port}`);
});