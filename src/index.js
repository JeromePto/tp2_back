const express = require("express");
const path = require('path');

const metrics = require("./metrics.js");

const app = express();

app.set('port', "8080")
app.set("views", path.join(__dirname, 'views'));
app.set("view engine", 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

app.get("/hello/:name", (req, res) => {
    // res.send("Hello " + req.params.name);
    res.render('hello.ejs', {name: req.params.name});
});

app.get('/metrics.json', (req, res) => {
    metrics.get((err, data) => {
        if (err) throw err;
        res.status(200).json(data);
    })
});

app.get('/', (req, res) => {
    res.write("Hello world");
    res.end();
});

app.listen(app.get('port'), (err) => {
    if (err) {
        throw err;
    }
    console.log(`server listening on ${app.get('port')}`);
});